package main

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

const secretKey string = "something"

func createToken(user User) (string, error) {
	// create the token
	token := jwt.New(jwt.SigningMethodHS256)

	// set some claims
	token.Claims = jwt.MapClaims{
		"id":       user.ID,
		"username": user.Name,
		"password": user.Password,
		"exp":      time.Now().Add(time.Hour * 24).Unix(),
	}

	//Sign and get the complete encoded token as string
	return (token.SignedString([]byte(secretKey)))
}

func validate(protectedPage http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenStr := strings.Replace(r.Header.Get("Authorization"), "Bearer ", "", 1)
		token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
			return []byte(secretKey), nil
		})

		if err == nil && token.Valid {
			protectedPage(w, r)
		} else {
			fmt.Printf("This token is bad! %v\n", token)
			w.Write([]byte("Bad token!"))
			return
		}

	})
}
