package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"github.com/dgrijalva/jwt-go"
)

var todos []Todo

func login(w http.ResponseWriter, r *http.Request) {
	var userFromReq User
	err := json.NewDecoder(r.Body).Decode(&userFromReq)
	if err != nil {
		panic(err)
	}

	var userFromDb User
	db.Where("name = ?", userFromReq.Name).First(&userFromDb)

	data := make(map[string]interface{})
	if userFromDb.Password != userFromReq.Password {
		data["success"] = "false"
		data["message"] = "User or password aren't valid"
	} else {
		token, _ := createToken(userFromDb)
		db.Where(&Todo{UserId: userFromDb.ID}).Find(&todos)
		data["success"] = "true"
		data["message"] = "Enjoy your token"
		data["todos"] = todos
		data["token"] = token
	}

	jdata, err := json.Marshal(data)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jdata)
}

func register(w http.ResponseWriter, r *http.Request) {
	newUser := new(User)
	err := json.NewDecoder(r.Body).Decode(&newUser)
	if err != nil {
		panic(err)
	}
	fmt.Printf("new user is: %v\n", newUser)
	db.Create(newUser)

	token, _ := createToken(*newUser)
	fmt.Printf("token is %v\n", token)

	data := make(map[string]string)
	data["message"] = "Succesfully registered!"
	data["token"] = token

	jdata, err := json.Marshal(data)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jdata)
}

func addTodo(w http.ResponseWriter, r *http.Request) {
	newTodo := new(Todo)
	err := json.NewDecoder(r.Body).Decode(&newTodo)
	if err != nil {
		panic(err)
	}

	user := getUserClaims(r)
	newTodo.UserId = uint64(user["id"].(float64))

	fmt.Printf("new todo is: %v\n", newTodo)
	db.Create(&newTodo)
	db.Where(&Todo{UserId: newTodo.UserId}).Find(&todos)

	jData, err := json.Marshal(todos)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jData)
}

func deleteTodo(w http.ResponseWriter, r *http.Request) {
	todoId, _ := strconv.ParseUint(mux.Vars(r)["todoId"], 0, 64)
	todo := new(Todo)
	db.Where(&Todo{ID: todoId}).First(&todo)

	user := getUserClaims(r)
	userId := uint64(user["id"].(float64))

	if todo.UserId != userId {
		w.Write([]byte("You can't delete another user todos!"))
	} else {
		db.Delete(&todo)
		db.Where(&Todo{UserId: userId}).Find(&todos)
		jData, _ := json.Marshal(todos)
		w.Header().Set("Content-Type", "application/json")
		w.Write(jData)
	}
}

func getUserClaims(r *http.Request) jwt.MapClaims {
	tokenStr := strings.Replace(r.Header.Get("Authorization"), "Bearer ", "", 1)
	token, _ := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		return []byte(secretKey), nil
	})
	return token.Claims.(jwt.MapClaims)
}
