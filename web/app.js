;(function(){
    function authInterceptor(API, auth) {
        return {
            // automatically attach Authorization header
            request: function(config) {
                var token = auth.getToken();
                console.log('in request; token is ' + token);
                if(token) {
                    config.headers.Authorization = 'Bearer ' + token;
                }

                return config;
            },
            // If a token was sent back, save it
            response: function(res) {
                console.log('in responce; token is ' + res.data.token);
                if(res.data.token) {
                    auth.saveToken(res.data.token);
                }

                return res;
            },
        }
    }

    function authService($window) {
        var self = this;

        // Add JWT methods here
        self.parseJwt = function(token) {
            var base64Url = token.split('.')[1];
            var base64 = base64Url.replace('-', '+').replace('_', '/');
            return JSON.parse($window.atob(base64));
        };

        self.saveToken = function(token) {
            $window.localStorage['jwtToken'] = token;
        };
        self.getToken = function() {
            return $window.localStorage['jwtToken'];
        };

        self.isAuthed = function() {
            var token = self.getToken();
            if(token) {
                var params = self.parseJwt(token);
                return Math.round(new Date().getTime() / 1000) <= params.exp;
            } else {
                return false;
            }
        };

        self.logout = function() {
            $window.localStorage.removeItem('jwtToken');
        }
    }

    function userService($http, API, auth) {
        var self = this;

        // add authentication methods here
        self.register = function(username, password) {
            return $http.post(API + '/register', {
                username: username,
                password: password
            })
        };

        self.login = function(username, password) {
            return $http.post(API + '/login', {
                username: username,
                password: password
            })
                .success(function(data) {
                    $scope.formData = {}; // clear the form so our user is ready to enter another
                    $scope.todos = data.todos;
                })
        };


    }

// We won't touch anything in here
    function MainCtrl(user, auth, $window, $http, API, $scope) {
        var self = this;

        function handleRequest(res) {
            var token = res.data ? res.data.token : null;
            console.log('data ' + JSON.stringify(res.data))
            if(token) { console.log('JWT:', token); }
            self.message = res.data.message;
        }

        self.createTodo = function() {
            $http.post('/api/addTodo', {
                text: $scope.formData.text,
            })
                .success(function(data) {
                    $scope.formData = {}; // clear the form so our user is ready to enter another
                    $scope.todos = data;
                })
                .error(function(data) {
                    console.log('Error: ' + data);
                });
        };

        self.deleteTodo = function(id) {
            $http.post('/api/delete/' + id, {

            })
                .success(function(data) {
                    $scope.formData = {}; // clear the form so our user is ready to enter another
                    $scope.todos = data;
                })
                .error(function(data) {
                    console.log('Error: ' + data);
                });
        };

        self.login = function() {
            user.login(self.username, self.password)
                .then(function (res) {
                    self.message = res.data.message;
                    $scope.formData = {};
                    $scope.todos = res.data.todos;
                });

        };
        self.register = function() {
            user.register(self.username, self.password)
                .then(handleRequest, handleRequest)
        };
        self.logout = function() {
            auth.logout && auth.logout()
        };
        self.isAuthed = function() {
            return auth.isAuthed ? auth.isAuthed() : false
        }
    }

    angular.module('app', [])
        .factory('authInterceptor', authInterceptor)
        .service('user', userService)
        .service('auth', authService)
        // .constant('API', 'http://test-routes.herokuapp.com')
        .constant('API', 'http://localhost:8080/api')
        .config(function($httpProvider) {
            $httpProvider.interceptors.push('authInterceptor');
        })
        .controller('Main', MainCtrl)
})();