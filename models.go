package main

//Models
type Todo struct {
	ID     uint64   `gorm:"AUTO_INCREMENT" gorm:"primary_key" json:"id"`
	Text   string   `json:"text"`
	UserId uint64
}

type User struct {
	ID       uint64 `gorm:"AUTO_INCREMENT" gorm:"primary_key" json:"id"`
	Name     string `gorm:"not null;unique" json:"username"`
	Password string `json:"password"`
	Todos    []Todo `gorm:"ForeignKey:UserRefer" json:"-"`
}
