package main

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var db *gorm.DB

func main() {
	initDb()
	r := mux.NewRouter()
	r.HandleFunc("/api/register", register)
	r.HandleFunc("/api/login", login)
	r.HandleFunc("/api/addTodo", validate(addTodo))
	r.HandleFunc("/api/delete/{todoId}", validate(deleteTodo))
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./web/")))
	http.Handle("/", r)
	http.ListenAndServe(":8080", r)
}

func initDb() {
	var err error
	db, err = gorm.Open("postgres", "postgresql://postgres:root@localhost:5432/todosGolang?sslmode=disable")
	if err != nil {
		panic(err)
	}

	// Ping function checks the database connectivity
	err = db.DB().Ping()
	if err != nil {
		panic(err)
	}

	//db.CreateTable(&User{})
	//db.CreateTable(&Todo{})
}
